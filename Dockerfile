FROM python:3.8-alpine
COPY requirements.txt /app/requirements.txt
RUN apk add --no-cache \
    bind-tools \
    libxml2 \
    libxml2-dev \
    libxslt-dev && \
    apk add --no-cache --virtual .build-deps \
    g++ \
    gcc && \
    pip install -r /app/requirements.txt && \
    apk add --no-cache --update curl ca-certificates && \
    apk del .build-deps

USER nobody

COPY . /app
WORKDIR /app
ENTRYPOINT python exporter.py
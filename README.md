# Areca Exporter

This is a Prometheus exporter for Areca RAID cards. 

It currently exports metrics about disks and the raid controller itself. It has been tested against an Areca ARC-1882I, firmware version V1.55 2018-03-23.

![Disk Temps](/misc/temps.png)

## Rationale

I was previously using SNMP and ArecaCLI to gather the metrics I needed. SNMP on this card has some limitations where it does not provide information about disks connected to another enclosure or SAS expander. ArecaCLI was very slow and difficult to get working in a docker environment. The method used by this exporter is to scrape the web UI for the needed information. Not only does this provide all of the metrics needed, it is much faster at it too.

# Deployment

This can be deployed solo or as a docker container. Docker images are provided in this gitlab repo [here](https://gitlab.com/josharrington/areca_exporter/container_registry). 

## Using the provided Docker Image 

You may use the prebuilt docker image provided by this repo. 

`docker pull registry.gitlab.com/josharrington/areca_exporter:v0.4`

Replace the tag version with the version you're interested in. You can find the versions in https://gitlab.com/josharrington/areca_exporter/-/tags

## Build Using Docker

1. Clone the project
2. In the project directory, run `docker build -t areca_exporter .`
3. Start with `docker run -e ARECA_ENDPOINT="http://arecahost" -e ARECA_USERNAME=admin -e ARECA_PASSWORD=password -t areca-exporter` and replace the values as appropriate

If you wish to create a SystemD unit file to run this as a service, see [misc/areca_exporter.service](misc/areca_exporter.service)

## Docker Compose

See [misc/docker-compose.example.yml](/misc/docker-compose.example.yml)

## Without Docker

1. Clone the project
2. Ensure you have Python 3.8+ installed. I recommend using [asdf](https://github.com/asdf-vm/asdf) to manage python versions on your system. This repo maintains a `.tool-versions` that is compatible with asdf.
3. Run `pip install -r requirements.txt`
4. Run `ARECA_ENDPOINT="http://arecahost" ARECA_USERNAME=admin ARECA_PASSWORD=password python exporter.py`

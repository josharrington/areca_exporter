from ArecaScraper import ArecaScraper
from prometheus_client import start_http_server
from prometheus_client.core import REGISTRY
import os 
import time 

if __name__ == '__main__':
    REGISTRY.register(ArecaScraper(
        start_url=os.environ.get('ARECA_ENDPOINT'),
        username=os.environ.get('ARECA_USERNAME'), 
        password=os.environ.get('ARECA_PASSWORD')
    ))
    start_http_server(5000)
    while True: time.sleep(1)
import requests
from requests.auth import HTTPDigestAuth
import re 
from bs4 import BeautifulSoup
from pprint import pprint
import os
from prometheus_client.core import GaugeMetricFamily, CounterMetricFamily

class ArecaScraper():
    start_url = ""
    session = requests.Session()
    drive_paths = []
    raid_set_paths = []
    drive_info = {}
    hw_info = {}
    default_prom_labels = {}
    raid_set_info = {}

    def __init__(self, start_url="", username="", password=""):
        self.start_url = start_url
        self.username = username
        self.password = password

        self.default_prom_labels = {
            "target": re.sub("^https?://", "", start_url)
        }

        self.start_session()

    def start_session(self):
        """
        Starts or restarts the session. The Areca may not be able to handle long sessions. Additionally, new logins invalidate old sessions.
        """
        # Dont freak out the server, start by hitting the main page. Areca is a bit finnicky
        self.session = requests.Session()
        self.session.auth = HTTPDigestAuth(self.username, self.password)
        response = self.session.get(self.start_url)

    def scrape_all(self):
        self.start_session()
        self.get_drive_urls()
        self.parse_all_drives()
        self.parse_hwinfo()
        self.get_raid_set_urls()
        self.parse_all_raid_sets()
        
    def get_drive_urls(self) -> list:
        # Then get the frame with the drive info
        response = self.session.get(self.start_url + '/hierarch.htm')

        if not response.ok:
            raise Exception('Did not get a 200')

        # Get drives, deduplicate
        self.drive_paths = list(set(
            [x.strip('"') for x in re.findall("\"ide[\w\d]+.html?\"", str(response.content))]
        ))

        return self.drive_paths

    def parse_all_drives(self):
        for drive in self.drive_paths:
            result = self.parse_drive(drive)
            self.drive_info[result['Serial Number']] = result

    def _rows_to_dict(self, rows):
        results = {}
        for row in rows: 
            aux = row.find_all('td')
            key = re.sub('\s+', ' ', aux[0].string.strip()) # Replace multiple spaces with one
            results[key] = aux[1].string.strip()
        return results

    def parse_drive(self, drive_path) -> dict:
        page = self.session.get(f"{self.start_url}/{drive_path}")
        soup = BeautifulSoup(page.content, features="lxml").find_all('table')[2]
        rows = soup.find_all('tr', {"bgcolor": "FFFFDB"})
        return self._rows_to_dict(rows)

    def parse_hwinfo(self) -> dict:
        # Only get Controller HW Monitor - excludes other enclosures (haven't thoroughly tested)
        page = self.session.get(f"{self.start_url}/hwmon.htm")
        soup = BeautifulSoup(page.content, features="lxml").find_all('table')[2].find('table')
        rows = soup.find_all('tr', {"bgcolor": "FFFFDB"})
        self.hw_info = self._rows_to_dict(rows)
        return self.hw_info

    def get_raid_set_urls(self) -> list:
        response = self.session.get(self.start_url + '/hierarch.htm')
        if not response.ok:
            raise Exception('Did not get a 200')
        
        self.raid_set_paths = list(set(
            [x.strip('"') for x in re.findall("\"rad[\w\d]+.html?\"", str(response.content))]
        ))
        
        return self.raid_set_paths

    def parse_all_raid_sets(self):
        for raid_set in self.raid_set_paths:
            result = self.parse_raid_set(raid_set)
            self.raid_set_info[result['Raid Set Name']] = result
    
    def parse_raid_set(self, raid_set_path) -> dict:
        page = self.session.get(f"{self.start_url}/{raid_set_path}")
        soup = BeautifulSoup(page.content, features="lxml").find_all('table')[1]
        rows = soup.find_all('tr', {"bgcolor": "FFFFDB"})
        return self._rows_to_dict(rows)

    def _get_prom_metric_family(self, metric_name, description, value, metric_family, labels={}):
        labels.update(self.default_prom_labels)
        mf = metric_family(
            name=metric_name,
            documentation=description,
            labels=labels.keys()
        )
        mf.add_metric(labels.values(), value)
        return mf

    def _get_prom_gauge(self, metric_name, description, value, labels={}):
        return self._get_prom_metric_family(metric_name, description, value, metric_family=GaugeMetricFamily, labels=labels)

    def _get_prom_counter(self, metric_name, description, value, labels={}):
        return self._get_prom_metric_family(metric_name, description, value, metric_family=CounterMetricFamily, labels=labels)

    def collect(self):
        self.scrape_all()

        ### Areca Card Hardware Metrics ###
        yield self._get_prom_gauge(
            'areca_cpu_temperature_celcius', 
            "Temperature of the Areca Card's CPU",
            value=re.search('^\d+', self.hw_info["CPU Temperature"]).group(0)
        )

        yield self._get_prom_gauge(
            'areca_controller_temperature_celcius', 
            "Temperature of the Areca Card's Controller",
            value=re.search('^\d+', self.hw_info["Controller Temp."]).group(0)
        )

        yield self._get_prom_gauge(
            'areca_cpu_fan_rpm', 
            "RPM of the CPU Fan",
            value=re.search('^\d+', self.hw_info["CPU Fan"]).group(0)
        )

        yield self._get_prom_gauge(
            'areca_voltage',
            "Voltage Information",
            value=re.search('^[\d\.]+', self.hw_info["12V"]).group(0),
            labels={"source": "12V"},
        )

        yield self._get_prom_gauge(
            'areca_voltage',
            "Voltage Information",
            value=re.search('^[\d\.]+', self.hw_info["5V"]).group(0),
            labels={"source": "5V"},
        )

        yield self._get_prom_gauge(
            'areca_voltage',
            "Voltage Information",
            value=re.search('^[\d\.]+', self.hw_info["3.3V"]).group(0),
            labels={"source": "3.3V"},
        )

        yield self._get_prom_gauge(
            'areca_voltage',
            'Voltage Information',
            labels={"source": "IO Voltage +1.8V"},
            value=re.search('^[\d\.]+', self.hw_info["IO Voltage +1.8V"]).group(0)
        )

        yield self._get_prom_gauge(
            'areca_voltage',
            'Voltage Information',
            labels={"source": "DDR3 +1.5V"},
            value=re.search('^[\d\.]+', self.hw_info["DDR3 +1.5V"]).group(0)
        )

        yield self._get_prom_gauge(
            'areca_voltage',
            'Voltage Information',
            labels={"source": "CPU VCore +1.0V"},
            value=re.search('^[\d\.]+', self.hw_info["CPU VCore +1.0V"]).group(0)
        )

        yield self._get_prom_gauge(
            'areca_voltage',
            'Voltage Information',
            labels={"source": "Analog +1.0V"},
            value=re.search('^[\d\.]+', self.hw_info["Analog +1.0V"]).group(0)
        )

        yield self._get_prom_gauge(
            'areca_voltage',
            'Voltage Information',
            labels={"source": "DDR3 +0.75V"},
            value=re.search('^[\d\.]+', self.hw_info["DDR3 +0.75V"]).group(0)
        )
        
        # Omitting battery status until a good strategy can be implemented. 
        # Without a battery, "Not Installed" is the value
        # yield self._get_prom_gauge(
        #     'areca_battery_status',
        #     'Areca Battery Status',
        #     value=re.search('^[\d\.]+', self.hw_info["Battery Status"]).group(0)
        # )

        ### End Areca Card Hardware Metrics ###

        ### Hard Drive Metrics ###
        for serial, info in self.drive_info.items():
            drive_labels = {
                'type': info['Device Type'],
                'location': info['Device Location'],
                'model': info['Model Name'],
                'serial': info['Serial Number'],
                'firmware': info['Firmware Rev.'],
                'capacity': info['Disk Capacity'],
                'rpm': info['Rotation Speed'],
            }

            yield self._get_prom_gauge(
                'areca_disk_temperature_celcius',
                'Disk Temperature',
                labels=drive_labels,
                value=re.search('^[\d\.]+', info['Device Temperature']).group(0)
            )

            yield self._get_prom_counter(
                'areca_disk_timeout_count_total',
                'Disk Timeout Count',
                labels=drive_labels,
                value=re.search('^[\d\.]+', info['Timeout Count']).group(0)
            )

            yield self._get_prom_counter(
                'areca_disk_media_error_count_total',
                'Disk Media Error Count',
                labels=drive_labels,
                value=re.search('^[\d\.]+', info['Timeout Count']).group(0)
            )

            # Smart data provided by Areca HTTP
            # Provided in format Attribute(Threshold)
            for attr in [
                'SMART Read Error Rate',
                'SMART Spinup Time',
                'SMART Reallocation Count',
                'SMART Seek Error Rate',
                'SMART Spinup Retries',
            ]:
                smart_value = re.search('^\d+', info[attr]).group(0)
                smart_threshold = re.search('\((\d+)\)', info[attr]).group(1)

                yield self._get_prom_gauge(
                    'areca_disk_smart_value',
                    'SMART Value',
                    labels={**drive_labels, "attribute": attr},
                    value=smart_value
                )

                yield self._get_prom_gauge(
                    'areca_disk_smart_threshold',
                    'SMART Threshold',
                    labels=drive_labels,
                    value=smart_threshold
                )

            if info['Device State'] == 'Normal':
                device_state = 1
            else:
                device_state = 0

            yield self._get_prom_gauge(
                'areca_disk_device_state_is_normal',
                'Disk state. 1 if normal.',
                labels=drive_labels,
                value=device_state
            )

        ### End Hard Drive Metrics ###

        ### Raid Set Metrics ### 
        for name, info in self.raid_set_info.items():
            raid_set_labels = {
                'name': info['Raid Set Name'],
            }

            yield self._get_prom_gauge(
                'areca_raid_set_disks_count',
                'Number of disks in the raid set',
                labels=raid_set_labels,
                value=info['Member Disks']
            )

            yield self._get_prom_gauge(
                'areca_raid_set_raw_capacity_bytes',
                'Number of raw bytes in the raid set',
                labels=raid_set_labels,
                value=float(info['Total Raw Capacity'].strip('GB')) * (1024**3)
            )

            yield self._get_prom_gauge(
                'areca_raid_set_free_capacity_bytes',
                'Number of free bytes in the raid set',
                labels=raid_set_labels,
                value=float(info['Free Raw Capacity'].strip('GB')) * (1024**3)
            )

            if info['Raid Set State'] == "Normal":
                raid_set_state = 1
            else:
                raid_set_state = 0
                
            yield self._get_prom_gauge(
                'areca_raid_set_state',
                'State of the raid set. 1 for normal',
                labels=raid_set_labels,
                value=raid_set_state
            )
        ### End Raid Set Metrics ###